# Jvalid

Lightweight tool to help keep your validators consistent and simple.

Check tests to see usage examples.


#### News
  
1.0.0  2021-07-06  

  * Deploy to jfrog artifactory repo.
  * Fix builder.  
  
0.0.1  2021-03-21  

  * Initial version.