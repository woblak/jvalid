package com.woblak.jvalidator;

public interface Validator<T1, T2> {

    T2 validate(T1 t);
}
