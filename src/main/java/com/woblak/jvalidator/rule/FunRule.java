package com.woblak.jvalidator.rule;

import java.util.function.Function;
import java.util.function.Predicate;

public class FunRule<T1, T2> {

    public final Predicate<T1> when;
    public final Function<T1, T2> thenFun;

    public FunRule(Predicate<T1> when, Function<T1, T2> thenFun) {
        this.when = when;
        this.thenFun = thenFun;
    }


    public static <T1, T2> FunRule.FunRuleBuilder<T1, T2> builder() {
        return new FunRule.FunRuleBuilder<>();
    }

    public static class FunRuleBuilder<T1, T2> {
        private Predicate<T1> when;
        private Function<T1, T2> thenFun;

        public FunRuleBuilder<T1, T2> when(Predicate<T1> when) {
            this.when = when;
            return this;
        }

        public FunRuleBuilder<T1, T2> thenFun(Function<T1, T2> thenFun) {
            this.thenFun = thenFun;
            return this;
        }

        public FunRule<T1, T2> build() {
            return new FunRule<>(when, thenFun);
        }
    }
}
