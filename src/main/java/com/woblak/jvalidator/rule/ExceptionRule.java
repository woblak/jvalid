package com.woblak.jvalidator.rule;

import java.util.function.Predicate;
import java.util.function.Supplier;

public class ExceptionRule<T> {

    public final Predicate<T> when;
    public final Supplier<? extends RuntimeException> thenExceptionSupp;

    public ExceptionRule(Predicate<T> when, Supplier<? extends RuntimeException> thenExceptionSupp) {
        this.when = when;
        this.thenExceptionSupp = thenExceptionSupp;
    }


    public static <T> ExceptionRule.ExceptionRuleBuilder<T> builder() {
        return new ExceptionRule.ExceptionRuleBuilder<>();
    }

    public static class ExceptionRuleBuilder<T> {
        private Predicate<T> when;
        private Supplier<? extends RuntimeException> thenExceptionSupp;

        public ExceptionRule.ExceptionRuleBuilder<T> when(Predicate<T> when) {
            this.when = when;
            return this;
        }

        public ExceptionRule.ExceptionRuleBuilder<T> thenExceptionSupp(
                Supplier<? extends RuntimeException> thenExceptionSupp
        ) {
            this.thenExceptionSupp = thenExceptionSupp;
            return this;
        }

        public ExceptionRule<T> build() {
            return new ExceptionRule<>(when, thenExceptionSupp);
        }
    }
}
