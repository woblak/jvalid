package com.woblak.jvalidator.rule;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class EventRule<T> {

    public final Predicate<T> when;
    public final Consumer<T> thenEvent;

    public EventRule(Predicate<T> when, Consumer<T> thenEvent) {
        this.when = when;
        this.thenEvent = thenEvent;
    }


    public static <T> EventRule.EventRuleBuilder<T> builder() {
        return new EventRule.EventRuleBuilder<>();
    }

    public static class EventRuleBuilder<T> {
        private Predicate<T> when;
        private Consumer<T> thenEvent;

        public EventRule.EventRuleBuilder<T> when(Predicate<T> when) {
            this.when = when;
            return this;
        }

        public EventRule.EventRuleBuilder<T> thenEvent(Consumer<T> thenEvent) {
            this.thenEvent = thenEvent;
            return this;
        }

        public EventRule<T> build() {
            return new EventRule<>(when, thenEvent);
        }
    }
}
