package com.woblak.jvalidator.rule;

import com.woblak.jvalidator.result.SimpleValidationStatus;

import java.util.function.Predicate;

public class SimpleRule<T> {

    public final Predicate<T> when;
    public final boolean thenMakeInvalid;
    public final String thenMsg;
    public final SimpleValidationStatus thenStatus;

    public SimpleRule(Predicate<T> when, boolean thenMakeInvalid, String thenMsg, SimpleValidationStatus thenStatus) {
        this.when = when;
        this.thenMakeInvalid = thenMakeInvalid;
        this.thenMsg = thenMsg;
        this.thenStatus = thenStatus;
    }


    public static <T> SimpleRule.RuleBuilder<T> builder() {
        return new SimpleRule.RuleBuilder<>();
    }

    public static class RuleBuilder<T> {

        private Predicate<T> when;
        private boolean thenMakeInvalid;
        private String thenMsg;
        private SimpleValidationStatus thenStatus;

        public SimpleRule.RuleBuilder<T> when(Predicate<T> when) {
            this.when = when;
            return this;
        }

        public SimpleRule.RuleBuilder<T> thenMakeInvalid(boolean valid) {
            thenMakeInvalid = valid;
            return this;
        }

        public SimpleRule.RuleBuilder<T> thenMsg(String thenMsg) {
            this.thenMsg = thenMsg;
            return this;
        }

        public SimpleRule.RuleBuilder<T> thenStatus(SimpleValidationStatus thenStatus) {
            this.thenStatus = thenStatus;
            return this;
        }

        public SimpleRule<T> build() {
            return new SimpleRule<>(when, thenMakeInvalid, thenMsg, thenStatus);
        }
    }
}
