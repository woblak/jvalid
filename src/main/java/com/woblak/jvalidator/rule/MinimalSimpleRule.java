package com.woblak.jvalidator.rule;

import java.util.function.Predicate;

public class MinimalSimpleRule<T> {

    public final Predicate<T> when;
    public final String thenMsg;

    public MinimalSimpleRule(Predicate<T> when, String thenMsg) {
        this.when = when;
        this.thenMsg = thenMsg;
    }


    public static <T> MinimalSimpleRule.SimpleRuleBuilder<T> builder() {
        return new MinimalSimpleRule.SimpleRuleBuilder<>();
    }

    public static class SimpleRuleBuilder<T> {

        private Predicate<T> when;
        private String thenMsg;

        public SimpleRuleBuilder<T> when(Predicate<T> when) {
            this.when = when;
            return this;
        }

        public SimpleRuleBuilder<T> thenMsg(String thenMsg) {
            this.thenMsg = thenMsg;
            return this;
        }

        public MinimalSimpleRule<T> build() {
            return new MinimalSimpleRule<>(when, thenMsg);
        }
    }
}
