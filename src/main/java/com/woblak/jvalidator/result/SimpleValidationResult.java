package com.woblak.jvalidator.result;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SimpleValidationResult {

    public final boolean isValid;
    public final SimpleValidationStatus status;
    public final List<String> messages;

    public SimpleValidationResult(boolean isValid, SimpleValidationStatus status, List<String> messages) {
        this.isValid = isValid;
        this.status = status;
        this.messages = Collections.unmodifiableList(messages);
    }


    public static SimpleValidationResult.ValidationResultBuilder build() {
        return new SimpleValidationResult.ValidationResultBuilder();
    }

    public static class ValidationResultBuilder {
        private boolean isValid;
        private SimpleValidationStatus status;
        private List<String> messages = new ArrayList<>();

        public ValidationResultBuilder isValid(boolean valid) {
            isValid = valid;
            return this;
        }

        public ValidationResultBuilder status(SimpleValidationStatus status) {
            this.status = status;
            return this;
        }

        public ValidationResultBuilder messages(List<String> messages) {
            this.messages = messages;
            return this;
        }

        public SimpleValidationResult build() {
            return new SimpleValidationResult(isValid, status, messages);
        }
    }
}
