package com.woblak.jvalidator.result;

public enum SimpleValidationStatus {

    OK,
    WARN,
    ERROR
}
