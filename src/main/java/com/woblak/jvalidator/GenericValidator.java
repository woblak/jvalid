package com.woblak.jvalidator;

import com.woblak.jvalidator.rule.EventRule;
import com.woblak.jvalidator.rule.ExceptionRule;
import com.woblak.jvalidator.rule.FunRule;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class GenericValidator<T1, T2> extends AbstractValidator<T1, List<T2>> {

    private final List<EventRule<T1>> eventRules = new ArrayList<>();
    private final List<ExceptionRule<T1>> exceptionRules = new ArrayList<>();
    private final List<FunRule<T1, T2>> funRules = new ArrayList<>();

    public static <X1, X2> GenericValidator<X1, X2> init() {
        return new GenericValidator<>();
    }

    public static <X1, X2> GenericValidator<X1, X2> of(Class<X1> clazz1, Class<X2> clazz2) {
        return new GenericValidator<>();
    }

    public List<T2> validate(T1 t) {
        executeEventRules(t);
        executeExceptionRules(t);
        return executeFunRules(t);
    }

    public GenericValidator<T1, T2> addRule(Predicate<T1> when, Consumer<T1> thenEvent) {
        EventRule<T1> rule = EventRule.<T1>builder().when(when).thenEvent(thenEvent).build();
        return addRule(rule);
    }

    public GenericValidator<T1, T2> addRule(EventRule<T1> rule) {
        this.eventRules.add(rule);
        return this;
    }

    public GenericValidator<T1, T2> addRule(Predicate<T1> when, Supplier<RuntimeException> thenExceptionSupp) {
        ExceptionRule<T1> rule = ExceptionRule.<T1>builder().when(when).thenExceptionSupp(thenExceptionSupp).build();
        return addRule(rule);
    }

    public GenericValidator<T1, T2> addRule(ExceptionRule<T1> rule) {
        this.exceptionRules.add(rule);
        return this;
    }

    public GenericValidator<T1, T2> addRule(Predicate<T1> when, Function<T1, T2> thenFun) {
        FunRule<T1, T2> rule = FunRule.<T1, T2>builder().when(when).thenFun(thenFun).build();
        return addRule(rule);
    }

    public GenericValidator<T1, T2> addRule(FunRule<T1, T2> rule) {
        this.funRules.add(rule);
        return this;
    }

    private void executeEventRules(T1 t) {
        eventRules.stream()
                .filter(r -> r.when.test(t))
                .forEach(r -> r.thenEvent.accept(t));
    }

    private void executeExceptionRules(T1 t) {
        Optional<ExceptionRule<T1>> exceptionRule = exceptionRules.stream()
                .filter(r -> r.when.test(t))
                .findAny();
        if (exceptionRule.isPresent()) {
            throw exceptionRule.get().thenExceptionSupp.get();
        }
    }

    private List<T2> executeFunRules(T1 t) {
        return funRules.stream()
                .filter(r -> r.when.test(t))
                .map(r -> r.thenFun.apply(t))
                .collect(Collectors.toList());
    }



}
