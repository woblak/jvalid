package com.woblak.jvalidator;

import com.woblak.jvalidator.result.SimpleValidationResult;
import com.woblak.jvalidator.result.SimpleValidationStatus;
import com.woblak.jvalidator.rule.EventRule;
import com.woblak.jvalidator.rule.ExceptionRule;
import com.woblak.jvalidator.rule.FunRule;
import com.woblak.jvalidator.rule.SimpleRule;
import com.woblak.jvalidator.rule.MinimalSimpleRule;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class SimpleValidator<T> extends AbstractValidator<T, SimpleValidationResult> {

    private final List<EventRule<T>> eventRules = new ArrayList<>();
    private final List<ExceptionRule<T>> exceptionRules = new ArrayList<>();
    private final List<SimpleRule<T>> simpleRules = new ArrayList<>();

    public static <X> SimpleValidator<X> init() {
        return new SimpleValidator<>();
    }

    public static <X> SimpleValidator<X> of(Class<X> clazz) {
        return new SimpleValidator<>();
    }

    @Override
    public SimpleValidationResult validate(T t) {
        executeEventRules(t);
        executeExceptionRules(t);
        return executeBasicRules(t);
    }

    public SimpleValidator<T> addRule(Predicate<T> when, Consumer<T> thenEvent) {
        EventRule<T> rule = EventRule.<T>builder().when(when).thenEvent(thenEvent).build();
        this.eventRules.add(rule);
        return this;
    }

    public SimpleValidator<T> addRule(EventRule<T> eventRule) {
        eventRules.add(eventRule);
        return this;
    }

    public SimpleValidator<T> addRule(Predicate<T> when, Supplier<RuntimeException> thenExceptionSupp) {
        ExceptionRule<T> rule = ExceptionRule.<T>builder().when(when).thenExceptionSupp(thenExceptionSupp).build();
        this.exceptionRules.add(rule);
        return this;
    }

    public SimpleValidator<T> addRule(ExceptionRule<T> exceptionRule) {
        exceptionRules.add(exceptionRule);
        return this;
    }

    public SimpleValidator<T> addRule(SimpleRule<T> simpleRule) {
        this.simpleRules.add(simpleRule);
        return this;
    }

    public SimpleValidator<T> addErrorRule(Predicate<T> when, String thenMsg) {
        MinimalSimpleRule<T> rule = MinimalSimpleRule.<T>builder().when(when).thenMsg(thenMsg).build();
        return addErrorRule(rule);
    }

    public SimpleValidator<T> addErrorRule(MinimalSimpleRule<T> minimalSimpleRule) {
        SimpleRule<T> simpleRule = SimpleRule.<T>builder()
                .when(minimalSimpleRule.when)
                .thenMsg(minimalSimpleRule.thenMsg)
                .thenMakeInvalid(true)
                .thenStatus(SimpleValidationStatus.ERROR)
                .build();
        this.simpleRules.add(simpleRule);
        return this;
    }

    public SimpleValidator<T> addWarnRule(Predicate<T> when, String thenMsg) {
        MinimalSimpleRule<T> rule = MinimalSimpleRule.<T>builder().when(when).thenMsg(thenMsg).build();
        return addWarnRule(rule);
    }

    public SimpleValidator<T> addWarnRule(MinimalSimpleRule<T> minimalSimpleRule) {
        SimpleRule<T> simpleRule = SimpleRule.<T>builder()
                .when(minimalSimpleRule.when)
                .thenMsg(minimalSimpleRule.thenMsg)
                .thenMakeInvalid(false)
                .thenStatus(SimpleValidationStatus.WARN)
                .build();
        this.simpleRules.add(simpleRule);
        return this;
    }

    public SimpleValidator<T> addMsgRule(Predicate<T> when, String thenMsg) {
        MinimalSimpleRule<T> rule = MinimalSimpleRule.<T>builder().when(when).thenMsg(thenMsg).build();
        return addMsgRule(rule);
    }

    public SimpleValidator<T> addMsgRule(MinimalSimpleRule<T> minimalSimpleRule) {
        SimpleRule<T> simpleRule = SimpleRule.<T>builder()
                .when(minimalSimpleRule.when)
                .thenMsg(minimalSimpleRule.thenMsg)
                .thenMakeInvalid(false)
                .thenStatus(SimpleValidationStatus.OK)
                .build();
        this.simpleRules.add(simpleRule);
        return this;
    }

    private void executeEventRules(T t) {
        eventRules.stream()
                .filter(r -> r.when.test(t))
                .forEach(r -> r.thenEvent.accept(t));
    }

    private void executeExceptionRules(T t) {
        Optional<ExceptionRule<T>> exceptionRule = exceptionRules.stream()
                .filter(r -> r.when.test(t))
                .findAny();
        if (exceptionRule.isPresent()) {
            throw exceptionRule.get().thenExceptionSupp.get();
        }
    }

    private SimpleValidationResult executeBasicRules(T t) {
        List<SimpleRule<T>> fulfilledSimpleRules = simpleRules.stream()
                .filter(r -> r.when.test(t))
                .collect(Collectors.toList());

        boolean isValid = getIsValidResult(fulfilledSimpleRules);
        SimpleValidationStatus status = getStatusResult(fulfilledSimpleRules);
        List<String> messages = getMessagesResult(fulfilledSimpleRules);

        return SimpleValidationResult.build()
                .isValid(isValid)
                .status(status)
                .messages(messages)
                .build();
    }

    private boolean getIsValidResult(List<SimpleRule<T>> fulfilledSimpleRules) {
        return fulfilledSimpleRules.stream().noneMatch(r -> r.thenMakeInvalid);
    }

    private SimpleValidationStatus getStatusResult(List<SimpleRule<T>> fulfilledSimpleRules) {
        SimpleValidationStatus result = fulfilledSimpleRules.stream().anyMatch(r -> SimpleValidationStatus.ERROR.equals(r.thenStatus)) ?
                SimpleValidationStatus.ERROR : SimpleValidationStatus.OK;
        if (SimpleValidationStatus.OK.equals(result)) {
            result = fulfilledSimpleRules.stream().anyMatch(r -> SimpleValidationStatus.WARN.equals(r.thenStatus)) ?
                    SimpleValidationStatus.WARN : SimpleValidationStatus.OK;
        }
        return result;
    }

    private List<String> getMessagesResult(List<SimpleRule<T>> fulfilledSimpleRules) {
        return fulfilledSimpleRules.stream().map(r -> r.thenMsg).collect(Collectors.toList());
    }
}
