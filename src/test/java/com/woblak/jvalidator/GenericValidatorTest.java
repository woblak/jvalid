package com.woblak.jvalidator;

import com.woblak.jvalidator.rule.ExceptionRule;
import com.woblak.jvalidator.rule.FunRule;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GenericValidatorTest {

    @Test
    void shouldExecOnlyFunRuleWhenFunRuleIsTrueAndExceptionRuleIsFalse() {
        // given
        int number = 1;
        int funRuleExpectedNum = 1;
        int excRuleExpectedNum = 0;

        // when
        GenericValidator<Integer, Double> validator = createGenericValidator(funRuleExpectedNum, excRuleExpectedNum);
        List<Double> results = validator.validate(number);

        // then
        assertEquals(1, results.size());
        assertEquals(0.5, results.get(0));
    }

    @Test
    void shouldNotExecAnyRuleWhenAllTrue() {
        // given
        int number = 1;
        int funRuleExpectedNum = 0;
        int excRuleExpectedNum = 0;

        // when
        GenericValidator<Integer, Double> validator = createGenericValidator(funRuleExpectedNum, excRuleExpectedNum);
        List<Double> results = validator.validate(number);

        // then
        assertEquals(0, results.size());
    }

    @Test
    void shouldThrowExceptionWhenExceptionRuleIsTrue() {
        // given
        int number = 1;
        int funRuleExpectedNum = 0;
        int excRuleExpectedNum = 1;

        // when
        GenericValidator<Integer, Double> validator = createGenericValidator(funRuleExpectedNum, excRuleExpectedNum);

        // then
        assertThrows(UnsupportedOperationException.class, () -> validator.validate(number));
    }

    @Test
    void shouldNotExecRulesWhenNoRules() {
        // given
        int number = 1;

        // when
        List<Double> results = GenericValidator.<Integer, Double>init()
                .validate(number);

        // then
        assertEquals(0, results.size());
    }


    private GenericValidator<Integer, Double> createGenericValidator(int i1, int i2) {
        FunRule<Integer, Double> whenFirstValueIsEqualToI1 = FunRule.<Integer, Double>builder()
                .when(i -> i == i1)
                .thenFun(i -> Double.valueOf(i) / 2.0)
                .build();
        ExceptionRule<Integer> whenFirstValueIsEqualToI2 = ExceptionRule.<Integer>builder()
                .when(i -> i == i2)
                .thenExceptionSupp(UnsupportedOperationException::new)
                .build();
        return GenericValidator.<Integer, Double>init()
                .addRule(whenFirstValueIsEqualToI1)
                .addRule(whenFirstValueIsEqualToI2);
    }

}

