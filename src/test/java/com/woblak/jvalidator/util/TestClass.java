package com.woblak.jvalidator.util;

import java.io.Serializable;

public class TestClass implements Serializable {

    public final int value1;
    public final int value2;

    public TestClass(int value1, int value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

}

