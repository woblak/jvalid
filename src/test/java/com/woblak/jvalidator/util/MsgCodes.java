package com.woblak.jvalidator.util;

public abstract class MsgCodes {

    public static final String NOT_POSITIVE = "Number 1 is not positive";
    public static final String NOT_EVEN = "Number 2 is not even";
    public static final String NOT_EQUAL = "Number 1 differs from number 2";
    public static final String ALL_THREE = "Numbers are equal to 3";
}
