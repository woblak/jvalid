package com.woblak.jvalidator;

import com.woblak.jvalidator.result.SimpleValidationResult;
import com.woblak.jvalidator.result.SimpleValidationStatus;
import com.woblak.jvalidator.rule.EventRule;
import com.woblak.jvalidator.rule.ExceptionRule;
import com.woblak.jvalidator.rule.SimpleRule;
import com.woblak.jvalidator.rule.MinimalSimpleRule;
import com.woblak.jvalidator.util.MsgCodes;
import com.woblak.jvalidator.util.TestClass;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SimpleValidatorTest {

    final SimpleValidator<TestClass> validator = createSimpleValidator();

    @Test
    void shouldReturnValidWhenAllRulesFalse() {
        // given
        TestClass t = new TestClass(1, 1);
        AtomicInteger ai = new AtomicInteger();

        // when
        EventRule<TestClass> eventRule = EventRule.<TestClass>builder()
                .when(i -> i.value1 == i.value2)
                .thenEvent(i -> ai.addAndGet(i.value1))
                .build();
        SimpleValidationResult result = validator.addRule(eventRule).validate(t);

        // then
        assertTrue(result.isValid);
        assertEquals(SimpleValidationStatus.OK, result.status);
        assertEquals(0, result.messages.size());
    }

    @Test
    void shouldReturnMessageWhenMessageRuleIsTrueAndOthersRulesAreFalse() {
        // given
        TestClass t = new TestClass(1, 3);

        // when
        SimpleValidationResult result = validator.validate(t);

        // then
        assertTrue(result.isValid);
        assertEquals(SimpleValidationStatus.OK, result.status);
        assertEquals(toString(MsgCodes.NOT_EQUAL), toString(result.messages));
    }

    @Test
    void shouldReturnWarningWhenWarningRuleIsTrueAndErrorRuleIsFalse() {
        // given
        TestClass t = new TestClass(1, 2);

        // when
        SimpleValidationResult result = validator.validate(t);

        // then
        assertTrue(result.isValid);
        assertEquals(SimpleValidationStatus.WARN, result.status);
        assertEquals(toString(MsgCodes.NOT_EQUAL, MsgCodes.NOT_EVEN), toString(result.messages));
    }

    @Test
    void shouldReturnErrorWhenErrorRuleIsTrue() {
        // given
        TestClass t = new TestClass(-1, 2);

        // when
        SimpleValidationResult result = validator.validate(t);

        // then
        assertFalse(result.isValid);
        assertEquals(SimpleValidationStatus.ERROR, result.status);
        assertEquals(toString(MsgCodes.NOT_EQUAL, MsgCodes.NOT_EVEN, MsgCodes.NOT_POSITIVE), toString(result.messages));
    }

    @Test
    void shouldValidateAccordingToExtendedRule() {
        // given
        TestClass t = new TestClass(3, 3);

        // when
        SimpleValidationResult result = validator.validate(t);

        // then
        assertFalse(result.isValid);
        assertEquals(SimpleValidationStatus.WARN, result.status);
        assertEquals(MsgCodes.ALL_THREE, toString(result.messages));
    }

    @Test
    void shouldThrowExceptionWhenExceptionRulesIsTrue() {
        // given
        TestClass t = new TestClass(2, 2);
        ExceptionRule<TestClass> shouldThrowExceptionRule = ExceptionRule.<TestClass>builder()
                .when(i -> i.value1 == 2)
                .thenExceptionSupp(UnsupportedOperationException::new)
                .build();

        // when
        SimpleValidator<TestClass> validatorWithExceptionRule = validator.addRule(shouldThrowExceptionRule);

        // then
        assertThrows(UnsupportedOperationException.class, () -> validatorWithExceptionRule.validate(t));
    }

    @Test
    void shouldReturnValidWhenNoRules() {
        // given
        TestClass t = new TestClass(2, 2);

        // when
        SimpleValidationResult result = SimpleValidator.<TestClass>init().validate(t);

        // then
        assertEquals(SimpleValidationStatus.OK, result.status);
        assertTrue(result.isValid);
        assertTrue(result.messages.isEmpty());
    }

    private SimpleValidator<TestClass> createSimpleValidator() {
        MinimalSimpleRule<TestClass> whenValue1IsNotPositive = MinimalSimpleRule.<TestClass>builder()
                .when(i -> i.value1 < 0)
                .thenMsg(MsgCodes.NOT_POSITIVE)
                .build();

        MinimalSimpleRule<TestClass> whenValue2IsNotEven = MinimalSimpleRule.<TestClass>builder()
                .when(i -> i.value2 % 2 == 0)
                .thenMsg(MsgCodes.NOT_EVEN)
                .build();

        MinimalSimpleRule<TestClass> whenValuesAreNotEqual = MinimalSimpleRule.<TestClass>builder()
                .when(i -> i.value1 != i.value2)
                .thenMsg(MsgCodes.NOT_EQUAL)
                .build();

        SimpleRule<TestClass> whenValuesAreEqualTo3 = SimpleRule.<TestClass>builder()
                .when(i -> i.value1 == 3 && i.value2 == 3)
                .thenMakeInvalid(true)
                .thenStatus(SimpleValidationStatus.WARN)
                .thenMsg(MsgCodes.ALL_THREE)
                .build();

        return SimpleValidator.of(TestClass.class)
                .addErrorRule(whenValue1IsNotPositive)
                .addWarnRule(whenValue2IsNotEven)
                .addMsgRule(whenValuesAreNotEqual)
                .addRule(whenValuesAreEqualTo3);
    }


    private String toString(String... strings) {
        return Arrays.stream(strings).sorted().collect(Collectors.joining());
    }

    private String toString(List<String> list) {
        return list.stream().sorted().collect(Collectors.joining());
    }
}
